<?php
/**
 * Created by IntelliJ IDEA.
 * User: shibafu
 * Date: 2015/10/11
 * Time: 3:40
 */

namespace Akari\Controller;


use Akari\Common\Constants;
use Akari\Logger\Logger;
use Smarty;

/**
 * コントローラの基本機能を提供します。
 * @package Akari\Controller
 */
class Controller
{
    /** @var Smarty */
    private $smarty;

    public function __construct()
    {
        $this->smarty = new Smarty();
        $this->smarty->assign('root_url', Constants::ROOT_URL);
    }

    protected function assign($key, $value) {
        $this->smarty->assign($key, $value);
    }

    public final function action($action_name)
    {

        // Check Callable
        if (!isset($this->{'allow_actions'}) || !in_array($action_name, $this->{'allow_actions'})) {
            Logger::warning($this, "Not Allowed : " . $action_name);
            return false;
        }

        // Call Function
        if (method_exists($this, $action_name)) {
            // Call Action
            $this->$action_name();
            // Call Render
            $this->smarty->display(dirname(__FILE__) . "/../View/" . $this->getControllerName() . "/" . $action_name . ".tpl");
            return true;
        } else {
            Logger::warning($this, "Action not found : " . $action_name);
            return false;
        }
    }

    private function getControllerName()
    {
        return preg_replace('/^Akari\\\\Controller\\\\(\S+)Controller$/', '$1', get_class($this));
    }
}