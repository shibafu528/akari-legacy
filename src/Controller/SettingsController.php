<?php
/**
 * Created by IntelliJ IDEA.
 * User: shibafu
 * Date: 2016/07/23
 * Time: 18:04
 */

namespace Akari\Controller;


use Akari\Common\Constants;
use Akari\Logger\Logger;
use Akari\Model\Filter;

class SettingsController extends Controller
{
    public $allow_actions = ['index', 'filter'];

    public function index()
    {
        // 現状では設定のメニューページを持たないので、なんとなくトップページに強制送還
        header('Location: ' . Constants::ROOT_URL);
        exit;
    }

    public function filter()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // パラメータを取得
            $filterId = (int) filter_input(INPUT_POST, 'filterId');
            $keyword = (string) filter_input(INPUT_POST, 'keyword');
            $target = (int) filter_input(INPUT_POST, 'target');
            $matcher = (int) filter_input(INPUT_POST, 'matcher');
            $delete = (bool) filter_input(INPUT_POST, 'delete');

            if (empty($filterId) && !empty($keyword)) {
                // 新規登録
                $filter = new Filter(0, $keyword, $target, $matcher, 0);
                $filter->save();

            } elseif (!empty($filterId) && !empty($keyword)) {
                // 更新

                /** @var Filter $filter */
                $filter = array_shift(Filter::find($filterId));

                if (is_null($filter)) {
                    Logger::warning($this, "[POST:filter] Filter not found -- filterId='$filterId'");

                } else {
                    $filter->keyword = $keyword;
                    $filter->target = $target;
                    $filter->matcher = $matcher;
                    $filter->save();
                }

            } elseif (!empty($filterId) && $delete === true) {
                // 削除
                Filter::delete($filterId);

            } else {
                // エラー
                Logger::warning($this, "[POST:filter] Invalid parameter -- filterId='$filterId', keyword='$keyword'");
            }

            header('Location: ' . Constants::ROOT_URL . '/settings/filter');
            exit;
        }

        // DBからデータを取得
        $filters = Filter::find();

        // ビューに設定
        $this->assign('data', $filters);
        $this->assign('target_label', Filter::TARGET_LABEL);
        $this->assign('matcher_label', Filter::MATCHER_LABEL);
    }
}