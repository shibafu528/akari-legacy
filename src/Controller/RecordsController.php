<?php
/**
 * Created by IntelliJ IDEA.
 * User: shibafu
 * Date: 2015/10/11
 * Time: 3:34
 */

namespace Akari\Controller;


use Akari\Common\Constants;
use Akari\Model\Filter;
use Akari\Model\Record;

class RecordsController extends Controller
{
    const ITEMS_PER_PAGE = 50;

    public $allow_actions = ['index', 'stream', 'update'];

    public function index()
    {
        // クエリを取得
        $query = (string)filter_input(INPUT_GET, 'filter');
        $quarter = (string)filter_input(INPUT_GET, 'quarter');
        $splitQuarter = explode('Q', $quarter);
        $page = (int)filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT);
        if ($page === 0) {
            $page = 1;
        }

        // データを取得
        $records = Record::find();
        $filters = Filter::find();

        // 四半期リストを生成
        $quarters = $this->getQuarterList($records);

        // フィルタ処理を行う
        $records = array_filter($records, function($v) use ($query, $splitQuarter, $filters) {
            // フィルタ設定を用いた判定
            foreach ($filters as $filter) {
                $target = '';

                // 検査対象を取得
                switch ($filter->target) {
                    case Filter::TARGET_TITLE:
                        $target = $v->title;
                        break;
                }

                // 判定を実行
                $visible = true;
                switch ($filter->matcher) {
                    case Filter::MATCHER_PARTIAL:
                        $visible = mb_strpos($target, $filter->keyword) === false;
                        break;
                    case Filter::MATCHER_EXACT:
                        $visible = $target !== $filter->keyword;
                        break;
                    case Filter::MATCHER_REGEX:
                        $visible = preg_match('/' . $filter->keyword . '/u', $target) !== 1;
                        break;
                }

                if ($visible === false) {
                    return false;
                }
            }

            // 四半期クエリを用いた判定
            if (!empty($splitQuarter) && count($splitQuarter) === 2) {
                if ($v->date->format('Y') !== $splitQuarter[0] || ($this->getQuarter($v->date) + 1) !== (int) $splitQuarter[1]) {
                    return false;
                }
            }

            // 検索クエリを用いた判定
            if (!empty($query)) {
                return mb_strpos($v->title, $query) !== false;
            }

            return true;
        });

        // 録画開始日時逆順ソート
        uasort($records, function ($lhs, $rhs) {
            if ($lhs->date == $rhs->date) {
                return 0;
            }
            return ($lhs->date < $rhs->date) ? 1 : -1;
        });

        // 件数を数えておく
        $recordsCount = count($records);
        $this->assign('count_all', $recordsCount);
        $this->assign('count_notyet', count(array_filter($records, function($x) { return !$x->watched; })));

        // ページネーション
        $records = array_slice($records, ($page - 1) * self::ITEMS_PER_PAGE, self::ITEMS_PER_PAGE);
        $this->assign('page', $page);
        $this->assign('page_count', ceil($recordsCount / self::ITEMS_PER_PAGE));

        // ビューに設定
        $this->assign('data', $records);
        $this->assign('filter', $query);
        $this->assign('quarter', $quarter);
        $this->assign('quarters', $quarters);
    }

    public function stream()
    {
        // クエリを取得
        $type = (string)filter_input(INPUT_GET, 'type');
        $container = (string)filter_input(INPUT_GET, 'container');
        $id = (string)filter_input(INPUT_GET, 'id');

        // タイプ指定がない場合はRAW
        if (empty($type)) {
            $type = 'raw';
        }

        // コンテナ指定がない場合はTS
        if (empty($container)) {
            $container = 'm2ts';
        }

        // ID指定がない場合は404返して打ち切り
        if (empty($id)) {
            header('HTTP/1.1 404 Not Found');
            exit;
        }

        // ファイル存在チェック
        if (!Record::existsFile($id)) {
            header('HTTP/1.1 410 Gone');
            exit;
        }

        // バッファのクリア
        while (ob_get_level()) {
            ob_end_clean();
        }

        // Chinachuサーバから取得、そのまま出力に流す
        if ($type == 'raw') {
            header("Transfer-encoding: chunked");
            switch ($container) {
                case 'mp4':
                    header("Content-Type: video/mp4");
                    $params = [
                        'ext' => 'mp4',
                        'c:v' => 'libx264',
                        'b:v' => '1M',
                        'c:a' => 'libfdk_aac',
                        'b:a' => '96k',
                        's' => '1280x720',
                        'ss' => '0'
                    ];
                    $endpoint = Constants::CHINACHU_URL . "/recorded/{$id}/watch.mp4?" . http_build_query($params);
                    break;
                case 'm2ts':
                    header("Content-Type: video/MP2T");
                    $endpoint = Constants::CHINACHU_URL . "/recorded/{$id}/watch.m2ts";
                    break;
            }
        } else {
            header("Content-Type: application/octet-stream");
            header("Content-Disposition: attachment; filename={$id}.xspf");
            $endpoint = Constants::CHINACHU_URL . "/recorded/{$id}/watch.xspf?";
            switch ($container) {
                case 'mp4':
                    $params = [
                        'ext' => 'mp4',
                        'c:v' => 'libx264',
                        'b:v' => '1M',
                        'c:a' => 'libfdk_aac',
                        'b:a' => '96k',
                        's' => '1280x720',
                        'prefix' => Constants::CHINACHU_URL . "/recorded/{$id}/"
                    ];
                    $endpoint .= http_build_query($params);
                    break;
                case 'm2ts':
                    $params = [
                        'ext' => 'm2ts',
                        'c:v' => 'copy',
                        'c:a' => 'copy',
                        'prefix' => Constants::CHINACHU_URL . "/recorded/{$id}/"
                    ];
                    $endpoint .= http_build_query($params);
                    break;
            }
        }

        if (empty($endpoint)) {
            header('HTTP/1.1 404 Not Found');
            exit;
        }

        readfile($endpoint);
    }

    public function update()
    {
        // パラメータを取得
        $program_id = (string) filter_input(INPUT_POST, 'programId');
        $new_value = (string) filter_input(INPUT_POST, 'newValue');
        $filter_value = (string) filter_input(INPUT_POST, 'filterValue');
        $quarter_value = (string) filter_input(INPUT_POST, 'quarterValue');
        $page_value = (string) filter_input(INPUT_POST, 'pageValue');

        // パラメータが不足している場合は打ち切り
        if ($program_id == '' || $new_value == '') {
            header('Location: ' . Constants::ROOT_URL . '/records');
            exit;
        }

        // DBからデータを取得
        $records = Record::find($program_id);

        // データが存在しない場合は打ち切り
        if (count($records) < 1) {
            header('Location: ' . Constants::ROOT_URL . '/records');
            exit;
        }

        // 視聴フラグを更新
        $records[0]->watched = $new_value;
        $records[0]->save();

        // インデックスページにリダイレクトして終了
        $query = [];
        if (!empty($filter_value)) {
            $query['filter'] = $filter_value;
        }
        if (!empty($quarter_value)) {
            $query['quarter'] = $quarter_value;
        }
        if (!empty($page_value)) {
            $query['page'] = $page_value;
        }
        header('Location: ' . Constants::ROOT_URL . '/records?' . http_build_query($query));
        exit;
    }

    /**
     * 四半期を表す文字列を取得します。
     * @param \DateTime $dateTime 対象のDateTime
     * @return int 四半期 (0～3)
     */
    private function getQuarter(\DateTime $dateTime)
    {
        $month = (int)$dateTime->format('n');
        return (int)(($month - 1) / 3);
    }

    /**
     * 録画データから四半期のリストを生成します。
     * @param Record[] $records 録画データ
     * @return string[] 四半期のリスト
     */
    private function getQuarterList(array $records)
    {
        $first = reset($records)->date;
        $last = end($records)->date;
        if ($first < $last) {
            $begin = $first;
            $end = $last;
        } else {
            $begin = $last;
            $end = $first;
        }

        $currentYear = (int) $begin->format('Y');
        $currentQuarter = $this->getQuarter($begin);
        $breakYearAndQuarter = $end->format('Y') . $this->getQuarter($end);

        $quarters = [];
        while ($currentYear . $currentQuarter <= $breakYearAndQuarter) {
            $quarters[] = $currentYear . 'Q' . ($currentQuarter + 1);

            $currentQuarter += 1;
            if ($currentQuarter > 3) {
                $currentYear += 1;
                $currentQuarter = 0;
            }
        }

        rsort($quarters);
        return $quarters;
    }
}