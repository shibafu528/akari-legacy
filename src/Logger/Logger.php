<?php
/**
 * Created by IntelliJ IDEA.
 * User: shibafu
 * Date: 2015/10/11
 * Time: 3:46
 */

namespace Akari\Logger;

/**
 * ログファイルへの出力を行う機能を提供します。
 * @package Akari\Logger
 */
class Logger
{
    /**
     * エラーログを出力します。
     * @param object $from
     * @param string $message
     */
    public static function warning($from, $message) {
        error_log("[" . get_class($from) . "] " . $message);
    }
}