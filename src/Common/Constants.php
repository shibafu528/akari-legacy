<?php
/**
 * Created by IntelliJ IDEA.
 * User: shibafu
 * Date: 2015/10/11
 * Time: 4:37
 */

namespace Akari\Common;

/**
 * アプリケーション内で共通して使用する定数宣言です。
 * @package Akari\Common
 */
class Constants
{
    const ROOT_URL = '/akari';
    const DEFAULT_CONTROLLER = 'Records';
    const CHINACHU_URL = 'http://192.168.11.26:20772/api';
}