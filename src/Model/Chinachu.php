<?php
/**
 * Created by IntelliJ IDEA.
 * User: shibafu
 * Date: 2015/10/11
 * Time: 19:22
 */

namespace Akari\Model;


use Akari\Common\Constants;
use Akari\Logger\Logger;
use Exception;

/**
 * Chinachuとの通信をラップしたクラスです。
 * @package Akari\Model
 */
class Chinachu
{
    /**
     * Chinachuサーバにリクエストを行い、結果として返されたJSONをデコードします。
     * @param string $endpoint エンドポイントURL
     * @param string $method HTTPリクエストメソッド (GET, POST)
     * @param array $post_fields 　POSTリクエスト時のデータ
     * @return array レスポンスの連想配列
     * @throws ChinachuException レスポンスコードが正常でない場合にスロー。
     */
    private static function requestJson($endpoint, $method = 'GET', $post_fields = [])
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, Constants::CHINACHU_URL . $endpoint);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        switch ($method) {
            case 'GET':
                curl_setopt($curl, CURLOPT_HTTPGET, true);
                break;
            case 'POST':
                curl_setopt($curl, CURLOPT_POST, true);
                curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_fields));
                break;
        }
        $response = curl_exec($curl);

        $header = curl_getinfo($curl);
        $code = $header['http_code'];
        curl_close($curl);
        if ($code >= 400) {
            // on error
            Logger::warning(Record::class, 'CURL error : ' . $response);
            throw new ChinachuException('CURL error : ' . $response, $code);
        }

        return json_decode($response, true);
    }

    /**
     * ChinachuサーバにGETリクエストを行い、結果として返されたJSONをデコードします。
     * @param string $endpoint エンドポイントURL
     * @return array レスポンスの連想配列
     * @throws ChinachuException レスポンスコードが正常でない場合にスロー。
     */
    public static function get($endpoint)
    {
        return self::requestJson($endpoint);
    }
}

class ChinachuException extends Exception
{
}