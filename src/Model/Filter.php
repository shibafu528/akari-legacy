<?php
/**
 * Created by IntelliJ IDEA.
 * User: shibafu
 * Date: 2016/07/24
 * Time: 2:11
 */

namespace Akari\Model;
use PDO;

/**
 * フィルタ設定のモデルです。
 * @package Akari\Model
 */
class Filter extends Model
{
    const TARGET_TITLE = 0;
    const MATCHER_PARTIAL = 0;
    const MATCHER_EXACT = 1;
    const MATCHER_REGEX = 2;

    /** 検査対象の表示名を表します。 */
    const TARGET_LABEL = [
        0 => '番組名'
    ];

    /** 一致条件の表示名を表します。 */
    const MATCHER_LABEL = [
        0 => '部分一致',
        1 => '完全一致',
        2 => '正規表現'
    ];

    /** @var int ID */
    public $id;
    /** @var String キーワード。 */
    public $keyword;
    /** @var int 検査対象。 */
    public $target;
    /** @var int 一致条件。 */
    public $matcher;
    /** @var int ユーザID。 */
    public $user_id;

    /**
     * Filter constructor.
     * @param int $id
     * @param String $keyword
     * @param int $target
     * @param int $matcher
     * @param int $user_id
     */
    public function __construct($id, $keyword, $target, $matcher, $user_id)
    {
        $this->id = $id;
        $this->keyword = $keyword;
        $this->target = $target;
        $this->matcher = $matcher;
        $this->user_id = $user_id;
    }

    /**
     * 現在の値をデータベースに保存します。
     */
    public function save()
    {
        $state = $this->getConnection()->prepare(
            'INSERT INTO filters (id, userId, keyword, target, matcher) ' .
            'VALUES (:id, :userId, :keyword, :target, :matcher) ' .
            'ON DUPLICATE KEY UPDATE keyword = :keyword, target = :target, matcher = :matcher, update_at = now()');
        $state->bindValue('id', $this->id, PDO::PARAM_INT);
        $state->bindValue('userId', $this->user_id, PDO::PARAM_INT);
        $state->bindValue('keyword', $this->keyword, PDO::PARAM_STR);
        $state->bindValue('target', $this->target, PDO::PARAM_INT);
        $state->bindValue('matcher', $this->matcher, PDO::PARAM_INT);
        $state->execute();
    }

    /**
     * データベースからレコードを取得します。
     * @param int|null $id ID
     * @param int $user_id ユーザID
     * @return Filter[] 検索結果
     */
    public static function find($id = null, $user_id = 0)
    {
        // DBと接続
        $pdo = self::beginConnection();
        $state = $pdo->prepare('SELECT id, userId, keyword, target, matcher FROM filters WHERE userId = :userId' . (is_null($id)? '' : ' AND id = :id'));
        $state->bindParam(':userId', $user_id, PDO::PARAM_INT);
        if (!is_null($id)) {
            $state->bindParam(':id', $id, PDO::PARAM_INT);
        }
        $state->execute();

        // データを取得
        $records = [];
        while ($row = $state->fetch(PDO::FETCH_ASSOC)) {
            $records[] = new Filter($row['id'], $row['keyword'], $row['target'], $row['matcher'], $row['userId']);
        }

        $pdo = null;

        return $records;
    }

    public static function delete($id, $user_id = 0)
    {
        $pdo = self::beginConnection();
        $state = $pdo->prepare('DELETE FROM filters WHERE id = :id AND userId = :userId');
        $state->bindParam(':id', $id, PDO::PARAM_INT);
        $state->bindParam(':userId', $user_id, PDO::PARAM_INT);
        $state->execute();
    }
}
