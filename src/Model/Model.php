<?php
/**
 * Created by IntelliJ IDEA.
 * User: shibafu
 * Date: 2015/10/12
 * Time: 2:31
 */

namespace Akari\Model;


use Akari\Common\Constants;
use Akari\Common\DatabaseConfig;
use Akari\Logger\Logger;
use PDO;
use PDOException;

/**
 * データベースアクセスなど、モデルの基本機能を提供します。
 * @package Akari\Model
 */
class Model
{
    /** @var PDO Connection */
    private $pdo = null;

    function __destruct()
    {
        $this->pdo = null;
    }

    protected function getConnection()
    {
        if (is_null($this->pdo)) {
            try {
                $this->pdo = self::beginConnection();
            } catch (PDOException $ex) {
                exit(1);
            }
        }
        return $this->pdo;
    }

    /**
     * データベースとの接続を開始します。
     * @return PDO
     */
    protected static function beginConnection()
    {
        try {
            return new PDO('mysql:dbname=' . DatabaseConfig::DB_CONNECTION_NAME .
                ';host=' . DatabaseConfig::DB_CONNECTION_HOST . ';charset=utf8',
                DatabaseConfig::DB_CONNECTION_USER,
                DatabaseConfig::DB_CONNECTION_PASS);
        } catch (PDOException $ex) {
            Logger::warning(static::class, "Database connection failed : " . $ex->getMessage());
            throw $ex;
        }
    }

}