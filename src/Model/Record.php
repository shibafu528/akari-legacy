<?php
/**
 * Created by IntelliJ IDEA.
 * User: shibafu
 * Date: 2015/10/11
 * Time: 16:04
 */

namespace Akari\Model;

use DateTime;
use DateTimeZone;
use PDO;

/**
 * 録画データの情報と視聴管理情報のモデルです。
 * @package Akari\Model
 */
class Record extends Model
{
    /** 録画データ一覧のAPIエンドポイント */
    const API_ENDPOINT = '/recorded';

    /** @var String Chinachu ID */
    public $program_id;
    /** @var String 番組名。 */
    public $title;
    /** @var String サブタイトル。 */
    public $subTitle;
    /** @var int 話数。 */
    public $episode;
    /** @var DateTime 放送日時。 */
    public $date;
    /** @var int ユーザID。 */
    public $user_id;
    /** @var bool 視聴フラグ。 */
    public $watched;

    /**
     * Record constructor.
     * @param string $program_id
     * @param string $title
     * @param string $subTitle
     * @param int $episode
     * @param DateTime $date
     * @param int $user_id
     * @param bool $watched
     */
    public function __construct($program_id, $title, $subTitle, $episode, DateTime $date, $user_id, $watched)
    {
        $this->program_id = $program_id;
        $this->title = $title;
        $this->date = $date;
        $this->user_id = $user_id;
        $this->watched = $watched;
        $this->subTitle = $subTitle;
        $this->episode = $episode;
    }

    public function save()
    {
        $state = $this->getConnection()->prepare(
            'INSERT INTO records (programId, userId, watched) ' .
            'VALUES (:programId, :userId, :watched) ' .
            'ON DUPLICATE KEY UPDATE watched = :watched, update_at = now()');
        $state->bindValue('programId', $this->program_id, PDO::PARAM_STR);
        $state->bindValue('userId', $this->user_id, PDO::PARAM_INT);
        $state->bindValue('watched', ($this->watched? '1' : '0'), PDO::PARAM_STR);
        $state->execute();
    }

    /**
     * @param null|string $program_id
     * @param int $user_id
     * @return Record[]
     */
    private static function findFromChinachu($program_id = null, $user_id = 0) {
        $records = [];

        try {
            if (is_null($program_id)) {
                $data = Chinachu::get(self::API_ENDPOINT . '.json');
                $previous = null;
                foreach ($data as $item) {
                    $record = self::parseJsonItem($item, $user_id);
                    if ($previous !== null && $previous == $record) {
                        continue;
                    }
                    $records[] = $record;
                    $previous = $record;
                }
            } else {
                $data = Chinachu::get(self::API_ENDPOINT . "/{$program_id}.json");
                $records[] = self::parseJsonItem($data, $user_id);
            }
        } catch (ChinachuException $ex) {}

        return $records;
    }

    private static function parseJsonItem($item, $user_id) {
        $datetime = new DateTime(null, new DateTimeZone('Asia/Tokyo'));
        $datetime->setTimestamp($item['start'] / 1000);
        return new Record($item['id'], $item['title'], $item['subTitle'], $item['episode'], $datetime, $user_id, false);
    }

    public static function find($program_id = null, $user_id = 0) {
        // Chinachuからデータを取得
        $records = self::findFromChinachu($program_id, $user_id);

        // DBと接続
        $pdo = self::beginConnection();
        $state = $pdo->prepare('SELECT programId, watched FROM records WHERE userId = :userId' . (is_null($program_id)? '' : ' AND programId = :programId'));
        $state->bindParam(':userId', $user_id, PDO::PARAM_INT);
        if (!is_null($program_id)) {
            $state->bindParam(':programId', $program_id, PDO::PARAM_STR);
        }
        $state->execute();

        // 視聴フラグデータを取得
        while ($row = $state->fetch(PDO::FETCH_ASSOC)) {
            foreach ($records as $record) {
                if ($record->program_id == $row['programId']) {
                    $record->watched = ($row['watched'] == '1');
                    break;
                }
            }
        }

        $pdo = null;

        return $records;
    }

    public static function existsFile($program_id) {
        try {
            Chinachu::get(self::API_ENDPOINT . "/{$program_id}/file.json");
            return true;
        } catch (ChinachuException $ex) {
            if ($ex->getCode() == 404 || $ex->getCode() == 410) {
                return false;
            }
            throw $ex;
        }
    }
}