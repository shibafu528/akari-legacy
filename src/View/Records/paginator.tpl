<div class="paginator">
    <ul class="paginator-pages">
        {strip}
            {if $page > 1}
                <li class="paginator-item"><a href="{$root_url}/records?quarter={$quarter}&filter={$filter}&page={max($page-1, 1)}">&laquo;</a></li>
                {for $i=max($page - 5, 1) to max($page - 1, 1)}
                    <li class="paginator-item"><a href="{$root_url}/records?quarter={$quarter}&filter={$filter}&page={$i}">{$i}</a></li>
                {/for}
            {/if}
            <li class="paginator-item current"><a href="{$root_url}/records?quarter={$quarter}&filter={$filter}&page={$page}">{$page}</a></li>
            {if $page < $page_count}
                {for $i=min($page + 1, $page_count) to min($page + 5, $page_count)}
                    <li class="paginator-item"><a href="{$root_url}/records?quarter={$quarter}&filter={$filter}&page={$i}">{$i}</a></li>
                {/for}
                <li class="paginator-item"><a href="{$root_url}/records?quarter={$quarter}&filter={$filter}&page={min($page+1, $page_count)}">&raquo;</a></li>
            {/if}
        {/strip}
    </ul>
</div>