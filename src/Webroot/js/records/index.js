/**
 * Created by shibafu on 2015/10/12.
 */

/**
 * POST /records/update
 * @param {string} programId
 * @param {string} newValue
 */
function update(programId, newValue) {
    $('#programId').val(programId);
    $('#newValue').val(newValue);
    $('#filterValue').val($('#searchFilter').val());
    $('#quarterValue').val($('#quarter').val());
    $('#pageValue').val(new URL(location.href).searchParams.get('page'));
    $('#form').submit();
}

function filter() {
    var isHiddenWatched = $('#hiddenWatched').prop('checked');
    $('.grid tr').each(function () {
        if (isHiddenWatched && $(this).find('.btn-watched').length > 0) {
            $(this).hide();
        } else {
            $(this).show();
        }
    });
    sessionStorage.setItem('hiddenWatched', isHiddenWatched);
}

$(function () {
    $('#hiddenWatched').prop('checked', sessionStorage.getItem('hiddenWatched') === 'true');
    filter();
});