<?php
/**
 * Created by IntelliJ IDEA.
 * User: shibafu
 * Date: 2015/10/11
 * Time: 4:51
 */
require '../../vendor/autoload.php';

use Akari\Common\Constants;
use Akari\Controller\Controller;

$query_remove_pattern = '/^(\w+)(\\?.*)?$/';
$params = explode('/', str_replace(Constants::ROOT_URL . '/', '', $_SERVER['REQUEST_URI']));

$controller_name = Constants::DEFAULT_CONTROLLER;
if (count($params) < 1 || $params[0] == '') {
    header('HTTP/1.1 302 Found');
    header('Location: ' . Constants::ROOT_URL . '/' . lcfirst(Constants::DEFAULT_CONTROLLER));
    exit;
}
$controller_name = ucfirst(preg_replace($query_remove_pattern, '$1', $params[0]));

$action_name = 'index';
if (count($params) > 1) {
    $action_name = preg_replace($query_remove_pattern, '$1', $params[1]);
}

$controller_class_name = 'Akari\\Controller\\' . $controller_name . 'Controller';
if (class_exists($controller_class_name)) {
    /** @var Controller $controller */
    $controller = new $controller_class_name();

    if ($controller->action($action_name)) {
        exit;
    }
}

// if not exist controller or action
header('HTTP/1.1 404 Not Found');
